#!/usr/bin/env python

import argparse
import os
import re
import subprocess
import tempfile

from pprint import pprint

user_lists = ['pgsql-admin', 'pgsql-advocacy', 'pgsql-announce', 'pgsql-bugs', 'pgsql-docs', 'pgsql-general', 'pgsql-interfaces', 'pgsql-jobs', 'pgsql-novice', 'pgsql-performance', 'pgsql-php', 'pgsql-sql', 'pgsql-students']

dev_lists = ['pgsql-cluster-hackers', 'pgsql-committers', 'pgsql-hackers', 'pgsql-rrreviewers', 'pgsql-www']

project_lists = ['pgadmin-hackers', 'pgadmin-support', 'pgsql-jdbc', 'pgsql-odbc', 'pgsql-pkg-debian', 'pgsql-pkg-yum', 'psycopg']

inactive_lists = ['pgsql-benchmarks', 'pgsql-chat', 'pgsql-cygwin', 'pgsql-hackers-pitr', 'pgsql-hackers-win32', 'pgsql-patches', 'pgsql-ports', 'pgsql-testers']

def parse_arguments():

    parser = argparse.ArgumentParser(description='Fetch and index PostgreSQL mailing list archives')

    parser.add_argument('--all-lists', dest='all_lists', action='store_true', help='all lists (user, project, devel, ...)')
    parser.add_argument('--user-lists', dest='user_lists', action='store_true', help='user lists (pgsql-admin, pgsql-general, ...)')
    parser.add_argument('--devel-lists',  dest='dev_lists',  action='store_true', help='developer lists (pgsql-hackers, pgsql-www, ...)')
    parser.add_argument('--project-lists',  dest='project_lists',  action='store_true', help='project lists (pgadmin-hackers, pgsql-jdbc, ...)')
    parser.add_argument('--inactive-lists',  dest='inactive_lists',  action='store_true', help='inactive lists (pgsql-benchmarks, pgsql-patches, ...)')
    parser.add_argument('--lists',  dest='lists',  nargs='+', metavar='LIST', action='store', help='other lists')

    return parser.parse_args()
    
def list_of_mboxes(mailing_list):
    
    r = fetch_url('http://www.postgresql.org/list/%s/' % (mailing_list))
    archives = {}
    for f in re.findall('%(list)s.[0-9]{6}' % {'list' : mailing_list}, r):
        archives.update({f : 0})

    return archives

def fetch_url(url, filename=None):

    tmpfile = tempfile.mkstemp()
    os.close(tmpfile[0])
    tmpfilename = tmpfile[1]

    subprocess.call(['wget', url, '-O', tmpfilename, '--http-user', 'archives', '--http-password', 'antispam'])

    if filename is None:
        f = open(tmpfilename, 'r')
        data = f.read()
        os.unlink(tmpfilename)
        return data
    else:
        os.rename(tmpfilename, filename)

def download_list_mboxes(mailing_list, downloaded):
    
    mboxes = list_of_mboxes(mailing_list)
    for mbox in mboxes:
        if (not downloaded.has_key(mbox)) or (downloaded[mbox] != mboxes[mbox]):
            print "fetching",mbox
            fetch_url('http://www.postgresql.org/list/%(list)s/mbox/%(mbox)s' % {'list' : mailing_list, 'mbox' : mbox}, '%(list)s/%(mbox)s' % {'list' : mailing_list, 'mbox' : mbox})
        else:
            print "skipping",mbox
    
    pass

def list_downloaded(mailing_list):
    
    files = {}
    for f in os.listdir(mailing_list):
        files.update({f : os.path.getsize('%s/%s' % (mailing_list, f))})
    
    return files

if __name__ == '__main__':
    
    args = parse_arguments()
    lists = []
    
    if args.all_lists:
        lists = user_lists + dev_lists + project_lists + inactive_lists
    
    if args.user_lists:
        lists += user_lists
        
    if args.dev_lists:
        lists += dev_lists
    
    if args.project_lists:
        lists += project_lists
    
    if args.inactive_lists:
        lists += inactive_lists

    if args.lists:
        lists += args.lists

    lists = set(lists)
    
    for l in lists:
        
        print "=====",l,"====="
        
        if not os.path.isdir(l):
            os.mkdir(l)
        
        downloaded = list_downloaded(l)
        
        download_list_mboxes(l, downloaded)
