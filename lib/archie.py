import cherrypy
import math
import psycopg2
import psycopg2.extras
import psycopg2.pool

class DB(object):
	'''connection context wrapper for the 'with' statement'''

	def __init__(self, pool):
		if pool == None:
			pool = psycopg2.pool.ThreadedConnectionPool(1, 10, cherrypy.config['db.connstr'])
		
		self._pool = pool

	def __enter__(self):
		self._conn = self._pool.getconn()
		return self

	def __exit__(self, type, value, traceback):
		self._pool.putconn(self._conn)

	def query(self, sql, params = None):
		try:
			cursor = self._conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
			cursor.execute(sql, params)
			data = cursor.fetchall()
			return data
		finally:
			cursor.close()

class ArchiveSearch(object):
    
	def __init__(self, pool):
		self._pool = pool

	def get_lists(self):
		'returns list of mailing lists within the database'
		
		with DB(self._pool) as db:
			return db.query("SELECT id, language FROM lists")

	def normalize_rank(self, results):
		
		if len(results) == 0:
			return []
		
		# find maximal rank
		maxrank = max([r['rank'] for r in results])
		
		# compute normalized ranks
		for r in results:
			r['rank_pct'] = math.ceil(r['rank'] * 1000 / maxrank)/10
		
		# returns 100 for maximal rank, percentage for other values
		return results

	def search_threads(self, query, limit = 100, datefrom = None, dateto = None):
		'searches by thread'
		
		with DB(self._pool) as db:
			
			# common parameters
			params = {'query' : query, 'limit' : limit}
			
			sql = """SELECT
						id, hash_id, rank, rank_pct, subject, author, sent,
						ts_headline('english', bar.body_plain, plainto_tsquery('english', %(query)s), 'StartSel=[highlight], StopSel=[/highlight], MinWords=100, MaxWords=255') AS body_plain
					FROM (
						SELECT messages.id, messages.hash_id, rank, 0 AS rank_pct, subject, author, sent, foo.body_plain
						FROM messages JOIN (
							SELECT thread_id AS id, string_agg(body_plain,' ') AS body_plain, SUM(ts_rank(body_tsvector, plainto_tsquery('english', %(query)s))) AS rank
							FROM messages WHERE body_tsvector @@ plainto_tsquery('english', %(query)s)"""
			
			# handle date range
			if datefrom and dateto:
				sql += " AND sent BETWEEN %(from)s AND %(to)s"
				params.update({'from' : datefrom, 'to' : dateto})
			elif datefrom:
				sql += " AND sent >= %(from)s"
				params.update({'from' : datefrom})
			elif dateto:
				sql += " AND sent <= %(to)s"
				params.update({'to' : dateto})
			
			sql = sql + """ GROUP BY thread_id
								) foo ON (foo.id = messages.id)
								ORDER BY rank DESC LIMIT %(limit)s) bar"""
			
			# fetch results (and normalize ranks)
			return self.normalize_rank(db.query(sql, params))

	def search(self, query, limit = 100, datefrom = None, dateto = None):
		'renders the search results page'
		
		with DB(self._pool) as db:
			
			# common parameters
			params = {'query' : query, 'limit' : limit}
			
			# basic SQL query
			sql = """SELECT
						id, hash_id, rank, rank_pct, subject, author, sent,
						  ts_headline('english', body_plain, plainto_tsquery('english', %(query)s), 'StartSel=[highlight], StopSel=[/highlight], MinWords=100, MaxWords=255') AS body_plain
					FROM (
						SELECT
							id, hash_id, ts_rank(body_tsvector, plainto_tsquery('english', %(query)s)) AS rank, 0 AS rank_pct, subject, author, sent, body_plain
						FROM messages
						WHERE body_tsvector @@ plainto_tsquery('english', %(query)s)"""
			
			# handle date range
			if datefrom and dateto:
				sql += " AND sent BETWEEN %(from)s AND %(to)s"
				params.update({'from' : datefrom, 'to' : dateto})
			elif datefrom:
				sql += " AND sent >= %(from)s"
				params.update({'from' : datefrom})
			elif dateto:
				sql += " AND sent <= %(to)s"
				params.update({'to' : dateto})
			
			# remaining part of the SQL query
			sql = sql + """ ORDER BY ts_rank(body_tsvector, plainto_tsquery('english', %(query)s)) DESC LIMIT %(limit)s) foo"""
			
			# fetch results (and normalize ranks)
			return self.normalize_rank(db.query(sql, params))

	def get_message_by_id(self, hash, query = None):
		'''get message by computed ID - hash of the whole message (including list name, so
		there's no point in adding list name as a parameter)'''
		
		with DB(self._pool) as db:
			
			if query == '' or query == None:
				data = db.query("""SELECT
									id, hash_id, list, message_id, subject, author, sent, rewrap(body_plain, 80) AS body_plain,
									(SELECT hash_id FROM messages WHERE id = m2.thread_id) AS thread_id
								FROM messages m2 WHERE hash_id = %(hash)s""", {'hash' : hash})
			else:
				data = db.query("""SELECT
									id, hash_id, list, message_id, subject, author, sent,
									ts_headline('english', rewrap(body_plain, 80), plainto_tsquery('english', %(query)s), 'StartSel=[highlight], StopSel=[/highlight], HighlightAll=TRUE') AS body_plain,
									(SELECT hash_id FROM messages WHERE id = m2.thread_id) AS thread_id
								FROM messages m2 WHERE hash_id = %(hash)s""", {'hash' : hash, 'query' : query})
			
			return data[0]

	def get_raw_message_by_id(self, hash):
		'''get raw message by computed ID - hash of the whole message (including list name, so
		there's no point in adding list name as a parameter)'''
		
		with DB(self._pool) as db:
			
			data = db.query("SELECT raw_message, (SELECT hash_id FROM messages WHERE id = m2.thread_id) AS thread_id FROM messages m2 WHERE hash_id = %(hash)s", {'hash' : hash})
			
			return data[0]

	def get_messages_by_mid(self, message_id, listname = None):
		'get messages by Message-ID (sent in header) - be careful as this may return multiple messages'
		
		with DB(self._pool) as db:
			if listname:
				return db.query("SELECT id, hash_id, subject, body_plain, thread_id FROM messages WHERE message_id = %(id)s AND list = %(list)s", {'id' : message_id, 'list' : listname})
			else:
				return db.query("SELECT id, hash_id, subject, body_plain, thread_id FROM messages WHERE message_id = %(id)s", {'id' : message_id})

	def get_references(self, hash):
		'get messages referenced by message with the given ID (hash)'
		
		with DB(self._pool) as db:
			return db.query("""SELECT
								r.parent_id AS id, m.hash_id, m.sent, m.subject
							FROM message_replies r JOIN messages m ON (r.parent_id = m.id) JOIN messages m2 ON (r.id = m2.id) WHERE m2.hash_id = %(hash)s""", {'hash' : hash})

	def get_thread(self, hash, query = None):
		'get thread (list of messages) by ID (hash)'
		
		with DB(self._pool) as db:
			
			if query == '' or query == None:
				return db.query("""SELECT
										id, hash_id, level, parent_id, subject, message_id, sent, author, rewrap(body_plain, 80) AS body_plain
									FROM messages WHERE thread_id = (SELECT id FROM messages WHERE hash_id = %(hash)s) ORDER BY sent, level""", {'hash' : hash})
			else:
				return db.query("""SELECT
										id, hash_id, level, parent_id, subject, message_id, sent, author,
										ts_headline('english', rewrap(body_plain, 80), plainto_tsquery('english', %(query)s), 'StartSel=[highlight], StopSel=[/highlight], HighlightAll=TRUE') AS body_plain
									FROM messages WHERE thread_id = (SELECT id FROM messages WHERE hash_id = %(hash)s) ORDER BY sent, level""", {'hash' : hash, 'query' : query})
